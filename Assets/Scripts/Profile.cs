﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Profile 
{
    [System.Serializable]
    private class MainData
    {
        public List<int> LevelStars;
        public int money = 10;
    }
    [System.Serializable]
    private class PlayerData
    {
        public bool sound = true;
        public bool music = true;
    }

    private static MainData mainData;
    private static PlayerData playerData;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckMainData()
    {
        Debug.Log("Init profile mainData");
        if(mainData!=null)
            return;
        if (!PlayerPrefs.HasKey("MainData"))
        {
            mainData=new MainData();
            PlayerPrefs.SetString("MainData",JsonUtility.ToJson(mainData));
            return;
        }

        mainData = JsonUtility.FromJson<MainData>(PlayerPrefs.GetString("MainData"));
    }
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckPlayerData()
    {
        Debug.Log("Init profile playerData");
        if(playerData!=null)
            return;
        if (!PlayerPrefs.HasKey("PlayerData"))
        {
            playerData=new PlayerData();
            PlayerPrefs.SetString("PlayerData",JsonUtility.ToJson(playerData));
            return;
        }

        playerData = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString("PlayerData"));
    }

    public static void Save(bool main = true, bool player = true)
    {
        if(main)
            PlayerPrefs.SetString("MainData",JsonUtility.ToJson(mainData));
        if(player)
            PlayerPrefs.SetString("PlayerData",JsonUtility.ToJson(playerData));
            
    }

    public static int Money
    {
        get=> mainData.money;
        set
        {
            mainData.money = value;
            if (mainData.money < 0)
            {
                mainData.money = 0;
            }
            Save(player:false);
        }
    }

    // public static int GetOpendedLevelCount => mainData.LevelStars.Count;
    //
    // public static int GetLevelStars(int level)
    // {
    //     if (level >= mainData.LevelStars.Count)
    //         return -1;
    //     return mainData.LevelStars[level];
    // }
    // public static void SetLevelStars(int level, int stars)
    // {
    //     if (level > mainData.LevelStars.Count)
    //     {
    //         Debug.LogError($"level {level} is not opened");
    //         return;
    //     }
    //
    //     if (level == mainData.LevelStars.Count)
    //     {
    //         stars = Mathf.Clamp(stars, 0, 3);
    //         mainData.LevelStars.Add(stars);
    //         Save(player:false);
    //     }
    //
    //     if (stars > mainData.LevelStars[level])
    //     {
    //         mainData.LevelStars[level] = stars;
    //         Save(player:false);
    //     }
    //         
    //     
    // }
    // [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    // private static void  CheckData <T>(T data) where T: new()
    // {
    //     Debug.Log($"Init profile {typeof(T)}Data");
    //     if(playerData!=null)
    //         return;
    //     if (!PlayerPrefs.HasKey(typeof(T).ToString()))
    //     {
    //         playerData=new T();
    //         PlayerPrefs.SetString("PlayerData",JsonUtility.ToJson(playerData));
    //         return;
    //     }
    //
    //     playerData = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString("PlayerData"));
    // }
}
