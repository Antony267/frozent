﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGizmos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        for (int y = 0; y < 10; y++)
        {
            for (int z = 0; z < 10; z++)
            {
                for (int x = 0; x < 10; x++)
                {
                    
                        // Draw a semitransparent blue cube at the transforms position
                        Gizmos.color = new Color(1, 0, 0, 0.5f);
                        Gizmos.DrawCube(transform.position, new Vector3(x, y, x));
                    
                }
            }
        }
       
        
    }

 
}
