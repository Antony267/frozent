﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PointsShowingOnSwipe : MonoBehaviour
{
    
    [SerializeField] private GameObject textPointPrefab;
    [SerializeField] private float timeRepeating=5f;
    [SerializeField] private float timer =3f;
    [SerializeField] private float speed = 0.5f;
    [SerializeField] private GameObject mainCamera;
    private static bool canShow = true;
    private float tempTimer = 0f;
    public void ShowPoints(Transform trans,int count)
    {
       
        tempTimer += Time.deltaTime;
        if (tempTimer >= timeRepeating)
        {
            tempTimer = 0f;
            canShow = true;
        }
        if (canShow)
        {
            canShow = false;
  
            var obj = Instantiate(textPointPrefab, trans.position, Quaternion.identity);
            obj.transform.rotation = mainCamera.transform.rotation;
            var textMeshPro = obj.GetComponent<TextMeshPro>();
            textMeshPro.fontSize = 4;
            textMeshPro.text = count.ToString();
        
            StartCoroutine(MoveText(obj.transform));
        }
    }

    
    public IEnumerator MoveText(Transform trans)
    {
        while (timer>0)
        {
            trans.Translate(speed*Time.deltaTime*Vector3.up , Space.World);
            timer -= Time.deltaTime;
            yield return null;
        }

        timer = 3f;
        Destroy(trans.gameObject);
    }
}
