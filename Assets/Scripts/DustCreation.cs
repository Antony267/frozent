﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustCreation : MonoBehaviour
{
    [SerializeField] private GameObject[] dustPrefabs;
    [SerializeField] private float radius=0.2f;
    [SerializeField] private float explosionForce = 20f;
    [SerializeField] private float explosionRadius = 5f;
    [SerializeField] private float dustLifeTime = 0.4f;

    public void CreateDust(Vector3 centrPoint)
    {
        
        foreach (var gameObj in dustPrefabs)
        {
           
            Vector3 randomPoint=centrPoint+new Vector3(Random.value-0.5f,Random.value-0.5f,Random.value-0.5f).normalized*radius;
            var obj = Instantiate(gameObj, randomPoint, Quaternion.Euler(GetRandomEulerVector()));
            if (obj.GetComponent<Rigidbody>() == null)
            {
                print("ins");
                var rig =obj.AddComponent<Rigidbody>();
                rig.AddExplosionForce(explosionForce,gameObj.transform.position,explosionRadius);
            
            }
               obj.GetComponent<Rigidbody>().AddExplosionForce(explosionForce,gameObj.transform.position,explosionRadius);
               Destroy(obj,dustLifeTime);
            
        }
    }
    private Vector3 GetRandomEulerVector()
    {
        return new Vector3(Random.Range(0,360),Random.Range(0,360),Random.Range(0,360));
    }
}
