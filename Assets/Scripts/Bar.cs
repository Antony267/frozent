﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{

    RectTransform rect;
    Vector2 defoultRect;
    float k;




    
    public void Init(float max)
    {
        rect = gameObject.GetComponent<RectTransform>();
        defoultRect = rect.sizeDelta;

        k = defoultRect.x / max;
    }

    public bool Modify(float now)
    {
        rect.sizeDelta = new Vector2(now * k, defoultRect.y);
        if (rect.sizeDelta.x <= 0) return true;
        else return false;
    }



}
