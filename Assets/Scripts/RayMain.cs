﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayMain : MonoBehaviour
{

    bool click = false;
    bool destroyBrash = false;
    [SerializeField] Camera cam;
    [SerializeField] GameObject sphereObj;

    void Start()
    {
        
    }


    void Update()
    {

        if (Input.GetMouseButton(0))
        {
            var touch = Input.mousePosition;

            Ray ray = cam.ScreenPointToRay(touch);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.collider.tag != "brash")
                {
                    destroyBrash = true;
                    GameObject colliderSphere = Instantiate(sphereObj);
                    colliderSphere.transform.position = hit.collider.transform.position;
                    gameObject.GetComponent<DustCreation>().CreateDust(hit.collider.transform.position);
                }
                else
                {
                    if (destroyBrash)
                    {
                        Destroy(hit.collider.gameObject);
                        destroyBrash = false;

                    }
                }
                    
                

            }
        }


    }



}
