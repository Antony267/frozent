﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIGame : MonoBehaviour
{

    public void Reload()
    {        
        SceneManager.LoadScene("Game");
    }

    public void InMenu()
    {
        SceneManager.LoadScene("Launch");
    }
}
