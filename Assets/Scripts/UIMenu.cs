﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMenu : MonoBehaviour
{
    [SerializeField] AudioClip audio1;

    [SerializeField] GameObject [] elemets = new GameObject[2];

    public void Play()
    {
        SceneManager.LoadScene("Game");
        gameObject.GetComponent<AudioSource>().PlayOneShot(audio1);
    }


    public void Settings()
    {
        elemets[0].SetActive(false);
        elemets[1].SetActive(true);
    }


    public void Android()
    {
        
    }
}
