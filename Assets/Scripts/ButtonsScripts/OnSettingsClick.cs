﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnSettingsClick : MonoBehaviour
{
    public void OpenSettingsScene()
    {
        SceneManager.LoadScene("SettingsScene");
    }
}
