﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnBackClick : MonoBehaviour
{
   [SerializeField] private GameObject googlePlayPanel;
   public void ClosePanel()
   {
      googlePlayPanel.SetActive(false);
   }

   public void ReturnToMainMenu()
   {
       SceneManager.LoadScene("LaunchScene");
   }

   private void OnMouseDown()
   {
       SceneManager.LoadScene("LaunchScene");
   }
}
