﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnNextClick : MonoBehaviour
{
    [SerializeField] private GameObject canvas;
    [SerializeField] private GameObject[] statuets;
    public void OnMouseDown()
    {
        for (int i = 0; i < statuets.Length; i++)
        {
            if (statuets[i].GetComponent<AudioSource>().isPlaying)
            {
                statuets[i].GetComponent<AudioSource>().Stop();
                statuets[i+1].GetComponent<AudioSource>().Play();
            }
        }
        if(canvas.activeSelf)
            canvas.SetActive(false);
       
    }

}
