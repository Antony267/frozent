﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnGooglePlayclick : MonoBehaviour
{
    [SerializeField] private GameObject googlePlayPanel;

    public void Start()
    {
        if(googlePlayPanel.activeSelf)
            googlePlayPanel.SetActive(false);
    }

    public void OpenPanel()
    {
        googlePlayPanel.SetActive(true);
    }
}
