﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPart : MonoBehaviour
{

    public List<GameObject> HaveTagContact = new List<GameObject>();


    public void ResetContacts(GameObject dump)
    {
        if (dump.CompareTag("contact")) HaveTagContact.Remove(gameObject);
        Destroy(dump);// --child;

        for (int i = 0; i < transform.childCount; i++) //Подготовка новой проверки на контакт
        {
            Part script = transform.GetChild(i).GetComponent<Part>();
            script.mainContact = script.complite = false;
            if (transform.GetChild(i).CompareTag("contact")) script.Recalculate(); //Подтвердить или опровергнуть контакт контактирующих с фигурой осколков.

            transform.GetChild(i).GetComponent<Collider>().isTrigger = false; 
        }


        foreach (GameObject h in HaveTagContact)  //Инициализация контактирующих с фигурой осколков.
        {

           if(h!=null) h.GetComponent<Part>().ReCheck();


        }
        Invoke("StartCheck", 1f); // задержка нужна чтобы успели сработать все OnTriggerEnter


        
    }


    void StartCheck()
    {

        for (int i = 0; i < transform.childCount; i++) //Запуск распространения волны проверки на контакт
        {

            transform.GetChild(i).GetComponent<Part>().CheckDestroy();

        }
    }





}
