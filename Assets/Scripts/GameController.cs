﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameController : MonoBehaviour
{
    [SerializeField] GameObject Win;
    [SerializeField] GameObject Lose;
    [SerializeField] GameObject GameUI;

    [SerializeField] GameObject barObj;
    [SerializeField] GameObject GameControllerObj;



    void Start()
    {

        StartCoroutine("Timer");

    }
    


    void Update()
    {
        if (transform.childCount < 100)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                
                transform.GetChild(i).GetComponent<Part>().mainContact = false;
                transform.GetChild(i).GetComponent<Part>().CheckDestroy();
            }

            StopCoroutine("Timer");
            GameControllerObj.GetComponent<RayMain>().enabled = false;
            GameUI.SetActive(false);
            Win.SetActive(true);

        }
    }

    

    IEnumerator Timer()
    {
        int time = 16;
        barObj.GetComponent<Bar>().Init(time);

        while (time>0)
        {
            yield return new WaitForSeconds(1);
            --time;
            barObj.GetComponent<Bar>().Modify(time);
        }

        GameControllerObj.GetComponent<RayMain>().enabled = false;
        GameUI.SetActive(false);
        Lose.SetActive(true);




    }

}
