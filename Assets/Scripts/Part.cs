﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : MonoBehaviour
{
    public List <GameObject> neighbors = new List<GameObject>();
    public bool mainContact = false;
    public bool complite = false;
    public bool testIsDestoy = false;
    public bool testIsChecked = false;

    void OnTriggerEnter(Collider collision)
    {
       
        GameObject go = collision.gameObject;
        if(go.tag != "brash" & go.tag != "other")
        {
            if(go.CompareTag("figure"))
            {
                mainContact = true;
                var obj = gameObject;
                obj.tag = "contact";
                gameObject.GetComponentInParent<ControlPart>().HaveTagContact.Add(obj);

            }
            else neighbors.Add(go);

            Check();
        }
            
        

    }



    public void Check() //проверяет есть ли соседи имеющие контакт с центром
    {
        if (!mainContact)
            foreach (GameObject chk in neighbors)
            {
                if (chk != null)
                {
                    if (chk.GetComponent<Part>().mainContact)
                    {
                        mainContact = true;
                        break;
                    }
                }
                
            } 
                   



    }


    public void ReCheck() //Запуск распространения волны проверки на контакт. Этот метод запускается только у контактирующих с фигурой осколков.
    {

            if (complite) return;
            complite = true; //чтобы не проверять один объект несколько раз

            mainContact = true;

            foreach (GameObject chk in neighbors)
            {
                if (chk != null)
                {
                    Part script = chk.GetComponent<Part>();
                    script.mainContact = true;
                    script.ReCheck();
                }
                
            }
        

    }


    public void CheckDestroy() 
    {

            if (!mainContact)
            {


                foreach (GameObject chk in neighbors) if(chk!=null)chk.GetComponent<Part>().neighbors.Remove(gameObject);

                if (gameObject.CompareTag("contact")) gameObject.GetComponentInParent<ControlPart>().HaveTagContact.Remove(gameObject);

                gameObject.GetComponent<Rigidbody>().isKinematic = false;
                gameObject.GetComponent<Rigidbody>().useGravity = true;
                //gameObject.GetComponent<Rigidbody>().WakeUp();



                Invoke("TotalDestroy",3f);
            
                

            }
    }

    void TotalDestroy() { Destroy(gameObject); }

    public void DestroyObj()
    {

        foreach (GameObject chk in neighbors) if (chk != null) chk.GetComponent<Part>().neighbors.Remove(gameObject);

        gameObject.GetComponentInParent<ControlPart>().ResetContacts(gameObject);  

    }

    public void Recalculate() //Поддтвердить контакт, если осколок касается фигуры. Используется при пересрасчете контактов после удаления объекта.
    {
        foreach (GameObject chk in neighbors)
        {
            if (chk != null) if (chk.CompareTag("figure")) { mainContact = true; break; }
        }
    }


 
    
}
