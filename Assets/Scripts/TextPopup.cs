﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextPopup : MonoBehaviour
{
    [SerializeField] private float timer =1f;
    [SerializeField] private float speed = 0.5f;
    private IEnumerator Start()
    {
        var pos = transform.position;
        pos-=new Vector3(2,0,2);
        transform.position = pos;
        while (timer>0)
        {
            transform.Translate(Vector3.up*speed, Space.World);
            timer -= Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }
}
